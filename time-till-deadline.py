from _datetime import datetime

user_input = input("Enter your goal with a deadline separated be a colon\n")
input_list = user_input.split(":")

goal = input_list[0]
deadline = input_list[1]

deadline_date = datetime.strptime(deadline, "%d.%m.%Y")
todays_date = datetime.today()
# Calculate how many days from now till the deadline
time_till = deadline_date - todays_date
hours_till = int(time_till.total_seconds() / 60 /60)

print(f"Dear user, time remaining for your goal: {goal} is {hours_till} hours")
