def days_to_units(num_of_days, conversion_unit):
    if conversion_unit == "hours":
        return f"{num_of_days} days contains {num_of_days * 24} hours"
    elif conversion_unit == "minutes":
        return f"{num_of_days} days contains {num_of_days * 24 * 60} minutes"
    else:
        return "unsuppoprted unit"


def validate_and_execute(days_and_unit_dictionary):
    try:
        user_input_number = int(days_and_unit_dictionary["days"])
        if user_input_number > 0:
            calculated_value = days_to_units(user_input_number, days_and_unit_dictionary["unit"])
            print(calculated_value)
        elif user_input_number == 0:
            print("Please enter a number greater than 0.")
        else:
            print("Your input must be an integer greater than zero.")
    except ValueError:
        print("Your input must be an integer greater than 0.")

user_input_message = "Enter the number of days and the conversion units.\n"